from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA
import pandas as pd
import numpy as np
from statsmodels.tsa.api import VAR
from sklearn.feature_selection import f_regression as f

data = pd.read_csv('processed_news_2.csv', index_col = "Date", parse_dates = True).fillna(0)
data = data.drop('None', axis = 1)
data = data.sort_index()
data = np.log(data.fillna(0) + 1).diff()
data = data.dropna()

vix = pd.read_csv('vixcurrent.csv', index_col = "Date", parse_dates = True)
vix = vix.dropna()
data['tar'] = vix.iloc[:,-1]

data = data.dropna()

train, test = data.iloc[:-100,:], data.iloc[-99:,:]

x_train, y_train = train.iloc[:,:-1], train.iloc[:,-1]
x_test, y_test = test.iloc[:,:-1], test.iloc[:,-1]

p, l = PCA(400), LinearRegression()
fv, pv = f(x_train, y_train)

best = [str(x) for x in np.argsort(pv)[-3:]] + ['tar']

# p.fit(x_train)

# l.fit(x_train, y_train)

# print(l.score(x_test, y_test))

# pred = l.predict(x_test)

import matplotlib.pyplot as plt
# y_test = y_test.to_frame()
# y_test['pred'] = pred

# y_test.plot();plt.show()



model = VAR(data[best])
r = model.fit(60)
# print(r.summary())

fore_i = r.forecast_interval(data[best].as_matrix()[:-100,:],100)

plt.plot(data[best].as_matrix()[-100:,-1])
plt.plot(fore_i[0][:,-1])
plt.plot(fore_i[1][:,-1])
plt.plot(fore_i[2][:,-1])
# plt.plot(data[best].as_matrix()[:-11,-1].max())
plt.show()

print(fore_i[0][:,-1])
print(fore_i[1][:,-1])
print(fore_i[2][:,-1])
data[best].as_matrix()[:-11,-1].max()
# model.plot_forecast(3)
# plt.show()