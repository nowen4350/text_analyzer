"""
This module will be used to take the (word,cluster_ID pair) and replace all words with their ID.
Then the data will be grouped by days the the distinct IDs counted. The count of the IDs will be put in their
own columns
"""

import pickle
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import RegexTokenizer
from pyspark.sql.types import StructType, StructField
from pyspark.sql.types import TimestampType, StringType, FloatType, ArrayType
from pyspark.sql.functions import from_unixtime, udf, collect_list, explode, first

def process_news(news_data, dic):
	#change unix to yyyy-MM-dd
	news_data = news_data.withColumn('Date', from_unixtime(news_data.Date, format = 'yyyy-MM-dd'))
	news_data = news_data.na.drop()

	tokenizer = RegexTokenizer(inputCol="Title", outputCol = 'Processed' \
		,pattern = "\\w+", gaps = False, minTokenLength = 2)
	news_data = tokenizer.transform(news_data)
	
	#now replace the words in the array with cluster IDs. If not found in the dictionary None will be the default
	func = udf(lambda ar: [str(dic.get(x)) for x in ar], ArrayType(StringType()))
	news_data = news_data.withColumn("Processed_ID", func(news_data.Processed))

	#count the number of cluster IDs per day.
	#Date 	| '0' 	| '1' 	|...
	#Wed  	|3000	|5000	|...
	#Thur 	|2000	|3000	|...
	news_data = news_data.select("Date", explode(news_data.Processed_ID).alias('ID')) \
	.groupBy('Date', 'ID') \
	.count().groupBy('Date') \
	.pivot('ID').agg(first('count'))

	return news_data

def replace_ID(cluster_file, news_file, save_loc):
	sc = SparkContext()
	spark = SQLContext(sc)

	#load the dictionary of (word, cluster_ID)
	dic = pickle.load(open(cluster_file, 'rb'))

	schema = StructType([
		StructField("Date", FloatType()),
		StructField("Title", StringType())])

	news_data = spark.read.csv(news_file, header = True, schema = schema)
	news_data = process_news(news_data, dic)

	news_data.toPandas().to_csv(save_loc, index = False)

	sc.stop()

replace_ID('Clusters/c500.p', '/media/nicholas/3EE47AECE47AA635/File_Downloads/news_data.csv', 'processed_news_2.csv')