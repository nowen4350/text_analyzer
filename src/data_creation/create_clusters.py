"""
This module will be used to assign each word in the glove vector data to a cluster
created by k-means. The intent is to reduce the dimensions of the data by putting like
words together.
"""
import pickle
from pyspark import SparkContext
from pyspark.mllib.clustering import KMeans

def split(line):
	line = line.split(' ')
	return (line[0], [float(x) for x in line[1:]])

def create_cluster(glove_file, clusters, iterations, save_loc):
	sc = SparkContext(appName = "ClusterMaker")

	data = sc.textFile(glove_file)
	parsedData = data.map(split)

	clusters = KMeans.train(parsedData.values(), clusters, maxIterations = iterations, initializationMode = 'random')

	final = parsedData.map(lambda item: (item[0], clusters.predict(item[1])))

	#turn into a dictionary
	final = final.collectAsMap()
	#save as a pickle
	pickle.dump(final, open(save_loc, 'wb'))

	sc.stop()
