# README #

### What is this repository for? ###

The purpose of this project is to take in headline data and observe the changes over time.

### Requirements ###

Python3
Pyspark
Pandas

### Included Works ###
The glove file is found at https://nlp.stanford.edu/projects/glove/
